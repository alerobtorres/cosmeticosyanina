/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cosmeticosyanina;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;



/**
 *
 * @author Martha
 */
public class CosmeticosYanina {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Clientes miCliente1=new Clientes();
        miCliente1.direccion="pacheco 567";
        miCliente1.nombre="Juan Perez";
        miCliente1.mail="juan@gmail.com";
        miCliente1.metodoPago="efectivo";
        
        
        Clientes miCliente2=new Clientes();
        miCliente2.nombre="alejandro";
        miCliente2.mail="alejandro@hotmail.com";
        miCliente2.metodoPago="debito";
        miCliente2.direccion="tamborini 564";
        
        
        Productos miProducto1=new Productos();
        miProducto1.nombre="lapiz labial";
        miProducto1.descripcion="color cereza";
        miProducto1.precio=678;
        
        
        Productos miProducto2=new Productos();
        miProducto2.descripcion="base para maquillaje";
        miProducto2.nombre="base";
        miProducto2.precio=890;
        
        
        Ventas miVenta1=new Ventas();
        miVenta1.miCliente=miCliente1;
        miVenta1.miProducto=miProducto1;
        miVenta1.cantidad=7;
        miVenta1.fecha="15/05/20";
       
        
        
        
        Ventas miVenta2=new Ventas();
        miVenta2.cantidad=3;
        miVenta2.fecha="5/04/20";
        miVenta2.miProducto=miProducto2;
        miVenta2.miCliente=miCliente2;
       
        
        VentaDetalle detalle1=new VentaDetalle();
        detalle1.miProducto=miProducto2;
        detalle1.cantidad=7;
        VentaDetalle detalle2=new VentaDetalle();
        detalle2.miProducto=miProducto1;
        detalle2.cantidad=98;
        
        miVenta1.listadoDeProductos.add(detalle1);
        miVenta1.listadoDeProductos.add(detalle2);
        
        System.out.println(miVenta1);
        System.out.println(miVenta2);
        
        //connection to database
        String usuario="cine2";
        String clave="cine";
        String url="jdbc:mysql://localhost:3306/cosmeticos_yanina";
        String consultaSql;
        consultaSql = "SELECT * FROM productos";
        Connection miConexion=null;
        System.out.println("conectandonos a la base de datos");
        
        try{
            miConexion=DriverManager.getConnection(url, usuario, clave);
            PreparedStatement miPreparacion=miConexion.prepareStatement(consultaSql);
            ResultSet miResultado=miPreparacion.executeQuery();
            while(miResultado.next()){
                System.out.println("Nombre: "+miResultado.getString("nombre")+" precio: "+miResultado.getString("precio")+
                        " descripcion: "+miResultado.getString("descripcion"));
            }
   
            }catch(SQLException miExepcion){
                System.out.println("---Alerta---");
                System.out.println(miExepcion);
            }finally{
                    try{
                    if(miConexion!=null){
                    miConexion.close();
                    }
                    }catch(SQLException ex){
                    System.out.println("problemas al cerrar la conexion con la base de datos");
                    System.out.println(ex);
                    }
                    }
            }
        }
      
        
    
    

